const esmangle = require("esmangle");
const _ = require("lodash");

module.exports = function (opts) {
  opts = opts || {};
  return function (override, transform) {
    transform("constructBundle", function (bundle) {
      var ast = bundle.ast;
      if (opts.optimize) {
        ast = esmangle.optimize(ast, null, { destructive: true });
      }
      ast = esmangle.mangle(ast);
      return _.extend({}, bundle, { ast });
    });
  };
};
