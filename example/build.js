var path = require("path");

var Interlock = require("interlock");
var esmangle = require("..");

var ilk = new Interlock({
  srcRoot: __dirname,
  destRoot: path.join(__dirname, "dist"),

  entry: { "./app/app.js": "app.bundle.js" },

  includeComments: true,
  sourceMaps: true,
  pretty: false,

  plugins: [ esmangle({ optimize: true }) ]
});

ilk.build()
